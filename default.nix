with import <nixpkgs> {};

let 
	der1 = derivation {
		name = "der1";
		builder = "${pkgs.bash}/bin/bash";
		cp = "${pkgs.coreutils}/bin";
		args = [ ./der1.sh ];
		system = builtins.currentSystem;
		make = "${pkgs.gnumake}/bin";
		makefile = ./Makefile;
		source = ./.;
		mkdir = "${pkgs.coreutils}/bin";
		gcc = "${pkgs.stdenv.cc}/bin";
	};
	der2 = derivation {
                name = "der2";
                builder = "${pkgs.bash}/bin/bash";
                cp = "${pkgs.coreutils}/bin";
                args = [ ./der2.sh ];
                system = builtins.currentSystem;
                make = "${pkgs.gnumake}/bin";
                makefile = ./Makefile;
                source = ./.;
                mkdir = "${pkgs.coreutils}/bin";
                gcc = "${pkgs.stdenv.cc}/bin";
		CXXFLAGS = "-fsanitize=address";
		buildInputs = [ catch2 ];
		includeFolder = "${pkgs.catch2}/include";
        };
	der3 = derivation {
		name = "der3";
                builder = "${pkgs.bash}/bin/bash";
                cp = "${pkgs.coreutils}/bin";
                args = [ ./der3.sh ];
                system = builtins.currentSystem;
                make = "${pkgs.gnumake}/bin";
                makefile = ./Makefile;
                source = ./.;
                mkdir = "${pkgs.coreutils}/bin";
                gcc = "${pkgs.stdenv.cc}/bin";
		afl = "${pkgs.afl}/bin";
	};
	der4 = deri: derivation {
		inherit deri;
                name = "der4";
                builder = "${pkgs.bash}/bin/bash";
                cp = "${pkgs.coreutils}/bin";
                args = [ ./der4.sh ];
                system = builtins.currentSystem;
                make = "${pkgs.gnumake}/bin";
                makefile = ./Makefile;
                source = ./.;
                mkdir = "${pkgs.coreutils}/bin";
                gcc = "${pkgs.stdenv.cc}/bin";
                afl = "${pkgs.afl}/bin";
        };


in  {
	der1_ = der1;
	der2_ = der2;
	der3_ = der3;
	der4_ = der4 der3;
}
