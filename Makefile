
CXXFLAGS+=-std=c++17

default: bin/main

bin/main: src/calculator.cpp src/main.cpp
	mkdir -p bin
	$(CXX) $(CXXFLAGS) -o $@ $^

bin/fuzz_main: src/calculator.cpp src/main.cpp
	mkdir -p bin
	afl-g++ $(CXXFLAGS) -o $@ $^

fuzz: bin/fuzz_main
	rm -rf test/out
	afl-fuzz -i test/in -o test/out -- ./bin/fuzz_main

clean:
	rm -rf bin
	rm -rf test/out

bin/tests: test/main.test.cpp src/main.cpp
	mkdir -p bin
	$(CXX) $(CXXFLAGS) -o $@ $^
	
